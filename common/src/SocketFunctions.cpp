//
// Created by udutta
//

#include "SocketFunctions.hpp"
#include <arpa/inet.h>
#include <cstring>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <sys/socket.h>
#include <unistd.h>

namespace common {

constexpr uint64_t maxMessageSize = 5242880;

bool sendMessage(const int connectedSocket, const std::string &msgToSend) {
  if (send(connectedSocket, msgToSend.c_str(), msgToSend.length(), 0) < 1) {
    std::cout << "!!! send() failed !!! " << std::endl;
    return false;
  }

  return true;
}

std::string recvMessage(int connectedSocket) {

  char recv_message[maxMessageSize];
  std::string recv_messageStr;

  memset(&recv_message, '\0', maxMessageSize);

  ssize_t numBytesRead = 0;
  ssize_t valReadCount = 0;

  while (true) {
    numBytesRead =
        recv(connectedSocket, &recv_message[valReadCount], maxMessageSize, 0);
    std::cout << "recvMessage >>   " << recv_message << std::endl;
    if (-1 == numBytesRead) {
      std::cout << "!!! Error reading message !!!" << std::endl;
      return "";
    }
    valReadCount += numBytesRead;

    std::cout << "Bytes read: " << numBytesRead << std::endl;
    std::cout << "Total bytes read: " << valReadCount << std::endl;

    recv_messageStr = recv_messageStr + recv_message;

    if (recv_messageStr.find("</xmlMessage>") != std::string::npos)
      break;
  }

  std::cout << ">> Received message: " << recv_messageStr << std::endl;
  std::cout << "--> Number of bytes read: " << valReadCount << std::endl;

  return recv_messageStr;
}

} // namespace common