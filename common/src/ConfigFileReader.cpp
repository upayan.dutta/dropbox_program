//
// Created by udutta
//

#include "ConfigFileReader.hpp"
#include "XmlMessageHandler.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

namespace common {

Config readConfigFile() {
  Config config;
  std::ifstream fileToOpen;
  std::stringstream ipAddress;
  std::stringstream port;
  fileToOpen.open("../config.ini");

  if (fileToOpen.is_open()) {
    std::cout << "Successfully opened config file" << std::endl;
  } else {
    std::cout << "!!! Error opening in Config file- Falling back to default "
                 "values !!!"
              << std::endl;

    return config;
  }

  std::string line;
  std::string content;
  while (std::getline(fileToOpen, line)) {
    content = content + line;
  }

  XmlMessageHandler fileParser;

  ipAddress << fileParser.getTagValue(content, "ipAddress");
  port << fileParser.getTagValue(content, "port");

  config.ipAdddress = ipAddress.str();
  port >> config.port;

  return config;
}

} // namespace common