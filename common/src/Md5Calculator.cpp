//
// Created by udutta
//

#include "Md5Calculator.hpp"
#include <iomanip>
#include <iostream>
#include <sstream>

namespace common {

unsigned char result[MD5_DIGEST_LENGTH];

std::string Md5Calculator::calculateMd5(const std::string &fileName) {
  std::cout << "In Md5Calculator::calculateMd5 for file: " << fileName
            << std::endl;

  unsigned long file_size;
  char *file_buffer;

  int fileDescriptor = open(fileName.c_str(), O_RDONLY);

  if (fileDescriptor < 0) {
    std::cout << "!!! Error in calculating MD5 for " << fileName << std::endl;
    return "";
  }

  file_size = get_size_by_fd(fileDescriptor);
  std::cout << "file size: " << file_size << std::endl;

  file_buffer = static_cast<char *>(
      mmap(0, file_size, PROT_READ, MAP_SHARED, fileDescriptor, 0));
  MD5((unsigned char *)file_buffer, file_size, result);
  munmap(file_buffer, file_size);

  return get_md5_sum(result);
}

unsigned long Md5Calculator::get_size_by_fd(const int fileDescriptor) {
  struct stat statbuf;

  if (fstat(fileDescriptor, &statbuf) < 0) {
    std::cout << "Error in getting file size" << std::endl;
  }
  return statbuf.st_size;
}

std::string Md5Calculator::get_md5_sum(unsigned char *md) {
  int i;
  std::ostringstream b;

  for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
    b << std::setfill('0') << std::setw(2) << std::setprecision(2) << std::hex
      << static_cast<unsigned int>(md[i]);
  }

  return b.str();
}

} // namespace common