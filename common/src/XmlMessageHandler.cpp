//
// Created by udutta
//

#include "XmlMessageHandler.hpp"
#include <iostream>
#include <sstream>

namespace common {
std::string XmlMessageHandler::createFinalmessage(const std::string &input) {
  std::string strToReturn = "<xmlMessage>" + input + "</xmlMessage>";

  // std::cout << strToReturn << std::endl;

  return strToReturn;
}

std::string XmlMessageHandler::createXmlTag(const std::string &tag,
                                            const std::string &content) {
  return "<" + tag + ">" + content + "</" + tag + ">";
}

std::string XmlMessageHandler::extractMessageType(const std::string &input) {
  std::cout << "In extractMessageType" << std::endl;
  std::string startTag{"<msgId>"};
  const auto startTagPos = input.find(startTag);
  const auto endTagPos = input.find("</msgId>");

  auto msgIdLen = endTagPos - (startTagPos + startTag.length());

  std::string msgType = input.substr(startTagPos + startTag.length(), msgIdLen);
  std::cout << "Extracted msg type: " << msgType << std::endl;
  return msgType;
}

std::string XmlMessageHandler::createInitialFileInfoFromServerRespMsg(
    const FileInformation &fileInfo) {
  std::ostringstream ss;

  ss << createXmlTag("msgId", "initialFileInfoFromServerResp");

  std::ostringstream size;
  size << fileInfo.size();
  ss << createXmlTag("noOfFiles", size.str());

  std::ostringstream fileList =
      createInitialFileInfoFromServerRespFileList(ss, fileInfo);

  ss << fileList.str();

  std::ostringstream final;
  final << createFinalmessage(ss.str());
  std::cout << "Upayan: " << final.str() << std::endl;

  return final.str();
}

std::ostringstream
XmlMessageHandler::createInitialFileInfoFromServerRespFileList(
    const std::ostringstream &msg, const FileInformation &fileInfo) {

  std::ostringstream ss;
  ss << "<fileList>";

  for (const auto &itr : fileInfo) {
    ss << "<listEntry>";

    ss << "<fileName>" + itr.first + "</fileName>";
    std::ostringstream fileSize;
    fileSize << itr.second.fileSize;
    ss << "<fileSize>" + fileSize.str() + "</fileSize>";
    ss << "<md5Sum>" + itr.second.md5Sum + "</md5Sum>";
    std::ostringstream lastWriteTime;
    lastWriteTime << itr.second.lastWriteTime;
    ss << "<lastWriteTime>" + lastWriteTime.str() + "</lastWriteTime>";

    ss << "</listEntry>";
  }
  ss << "</fileList>";

  return ss;
}

std::string XmlMessageHandler::getTagValue(const std::string &content,
                                           const std::string &xmlTag) const {

  std::string startTag = "<" + xmlTag + ">";
  std::string endTag = "</" + xmlTag + ">";

  const auto startTagPos = content.find(startTag);
  const auto endTagPos = content.find(endTag);

  auto msgIdLen = endTagPos - (startTagPos + startTag.length());

  std::string tagContent =
      content.substr(startTagPos + startTag.length(), msgIdLen);
  // std::cout << "Extracted content: " << tagContent << std::endl;
  return tagContent;
}

FileInformation XmlMessageHandler::getFileList(const std::string &content,
                                               const uint32_t numOfFiles) const
    try

{
  std::cout << "In getFileList" << std::endl;

  std::string tmpContent = content;
  uint32_t counter = 0;
  FileInformation fileInfo;

  while (counter < numOfFiles) {
    common::FileInfo fileInfoToSave;
    const std::string fileInfoStr = getTagValue(tmpContent, "listEntry");

    //    std::cout << " ===== " << fileInfoStr << " ===== " << std::endl;

    std::string fileName = getTagValue(fileInfoStr, "fileName");
    std::cout << "File name: " << fileName << std::endl;

    std::stringstream fileSizeStr;
    fileSizeStr << getTagValue(fileInfoStr, "fileSize");
    uintmax_t fileSize;
    fileSizeStr >> fileSize;
    fileInfoToSave.fileSize = fileSize;
    std::cout << "File size: " << fileInfoToSave.fileSize << std::endl;

    fileInfoToSave.md5Sum = getTagValue(fileInfoStr, "md5Sum");
    std::cout << "MD5 Sum: " << fileInfoToSave.md5Sum << std::endl;

    std::stringstream lastWriteTimeStr;
    lastWriteTimeStr << getTagValue(fileInfoStr, "lastWriteTime");
    time_t lastWriteTime;
    lastWriteTimeStr >> lastWriteTime;
    fileInfoToSave.lastWriteTime = lastWriteTime;
    std::cout << "Last write time: " << fileInfoToSave.lastWriteTime
              << std::endl;

    std::string listEntryStr = "<listEntry></listEntry>";

    tmpContent.erase(0, fileInfoStr.length() + listEntryStr.length());

    //    std::cout << "After erase ===== " << tmpContent << " ===== " <<
    //    std::endl;

    fileInfo.emplace(std::make_pair(fileName, fileInfoToSave));

    counter++;
  }

  return fileInfo;
} catch (...) {
  std::cout << "!!! Exception caught in getFileList !!!" << std::endl;
  return FileInformation{};
}

} // namespace common