//
// Created by udutta
//

#pragma once

#include "Types.hpp"
#include <map>
#include <string>

namespace common {

using FileInformation = std::map<std::string, common::FileInfo>;

class XmlMessageHandler {
public:
  std::string createFinalmessage(const std::string &input);
  std::string createXmlTag(const std::string &tag, const std::string &content);
  std::string extractMessageType(const std::string &input);
  std::string createInitialFileInfoFromServerRespMsg(const FileInformation &);
  std::string getTagValue(const std::string &content,
                          const std::string &xmlTag) const;
  FileInformation getFileList(const std::string &content,
                              uint32_t numOfFiles) const;

private:
  std::ostringstream
  createInitialFileInfoFromServerRespFileList(const std::ostringstream &,
                                              const FileInformation &);
};

} // namespace common