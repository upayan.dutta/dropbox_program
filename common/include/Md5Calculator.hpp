//
// Created by udutta
//

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <openssl/md5.h>

namespace common {

class Md5Calculator {

public:
  std::string calculateMd5(const std::string &fileName);

private:
  unsigned long get_size_by_fd(int fileDescriptor);
  std::string get_md5_sum(unsigned char *md);
};

} // namespace common
