//
// Created by udutta
//

#pragma once

#include <string>

namespace common {

struct FileLineInfo {
  std::string fileName;
  std::uintmax_t lineNumber;
};

struct FileInfo {
  uintmax_t fileSize;
  time_t lastWriteTime;
  bool isUploadedInServer{false};
  std::string md5Sum;
};

struct Config {
  std::string ipAdddress{"127.0.0.1"};
  std::uint16_t port{1234};
};

} // namespace common