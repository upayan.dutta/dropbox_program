//
// Created by udutta
//

#include <string>

namespace common {
bool sendMessage(int connectedSocket, const std::string &msgToSend);
std::string recvMessage(int connectedSocket);
} // namespace common
