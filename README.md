# Project Title

'DropBox' program- A simple CLI based client-server utility where:

A simple client takes one directory as an argument and keeps monitoring changes in that directory and uploads any change to its server.
A simple server which takes one empty directory as an argument and receives any change from its client.

## Getting Started

```
git clone git@gitlab.com:upayan.dutta/dropbox_program.git
```

## Prerequisites

* Linux 64-bit (Development Kernel version: 4.4.136)
* C++17 (Minimum GCC version required: 8.3.0)
* Cmake (Minimum version required: 3.12)

## Building

### Without Address Sanitizer

```
udutta@upayan-rhel ~ $ cd dropbox_program/
udutta@upayan-rhel ~ $ source source_environment.sh
udutta@upayan-rhel dropbox_program (master) $ mkdir build ; cd build
udutta@upayan-rhel build (master) $ cmake ..
udutta@upayan-rhel build (master) $ make all
```

### With Address Sanitizer

```
udutta@upayan-rhel ~ $ cd dropbox_program/
udutta@upayan-rhel ~ $ source source_environment.sh
udutta@upayan-rhel dropbox_program (master) $ mkdir build ; cd build
udutta@upayan-rhel build (master) $ cmake -DSANITIZER_ASAN=ON ..
udutta@upayan-rhel build (master) $ make all
```

## Detailed Documentation

```
doc/DropBox_v1.0.docx
```
