//
// Created by udutta
//

#pragma once

#include "../../common/include/Types.hpp"
#include "Md5Calculator.hpp"
#include "ServerMonitor.hpp"
#include <filesystem>
#include <iostream>
#include <map>

namespace application {
namespace server {
using FileInformation = std::map<std::string, common::FileInfo>;
namespace file = std::filesystem;

class ServerDirectoryMonitor : public ServerMonitor {
public:
  ServerDirectoryMonitor(std::string pathToMonitor)
      : directoryToMonitor{pathToMonitor} {}

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Saves file name and file size received from client
  // so that server can be ready for file transfer
  ///////////////////////////////////////////////////////////////////////
  void saveFileInformation(const std::string &fileName,
                           uintmax_t fileSize) override;

  void waitForFileFromClient(const std::string &fileName, uintmax_t fileSize,
                             const int &connectedSocket) override;

  bool sendSendFileNameSizeResp(int connectedSocket) override;

  bool handleInitialFileInformationMessage(int connectedSocket) override;

private:
  FileInformation fileInformation;
  FileInformation fileInformationToSendInInitialFileInformationMessage;
  std::string directoryToMonitor;

  void createFile(char *buffer, const std::string &fileName,
                  uintmax_t fileSize);

  bool sendFileUploadAckToClient(const int &connectedSocket);
  bool sendInitialFileInfoFromServerResp(int connectedSocket);
  void scanFilesInDirectory();
  std::uintmax_t getFileSize(const file::path &fileToCheck);
  void saveFileInformationToBeSent(const file::directory_entry &entry,
                                   const file::path &filename,
                                   common::Md5Calculator &);
};

} // namespace server
} // namespace application