//
// Created by udutta
//

#pragma once

#include <string>

namespace application {
namespace server {

class Manager {
public:
  virtual void start(std::string pathToScan) = 0;

  virtual ~Manager() = default;
};

} // namespace server
} // namespace application
