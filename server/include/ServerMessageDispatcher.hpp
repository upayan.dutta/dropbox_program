//
// Created by udutta
//

#pragma once

#include "../../common/include/Types.hpp"
#include "ServerMonitor.hpp"
#include "iostream"
#include <memory>

namespace application {
namespace server {

class ServerMessageDispatcher {
public:
  virtual void dispatch(const std::string &) = 0;
};

class FileNameSizeMessage : public ServerMessageDispatcher {
public:
  FileNameSizeMessage(int &socket, std::shared_ptr<ServerMonitor> serverMonitor)
      : connectedSocket{socket}, serverMonitor{serverMonitor} {}

  void dispatch(const std::string &) override;

private:
  int &connectedSocket;
  std::shared_ptr<ServerMonitor> serverMonitor;
};

class InitialFileInformationMessage : public ServerMessageDispatcher {
public:
  InitialFileInformationMessage(int &socket,
                                std::shared_ptr<ServerMonitor> serverMonitor)
      : connectedSocket{socket}, serverMonitor{serverMonitor} {}
  void dispatch(const std::string &) override;

private:
  int &connectedSocket;
  std::shared_ptr<ServerMonitor> serverMonitor;
};

} // namespace server
} // namespace application
