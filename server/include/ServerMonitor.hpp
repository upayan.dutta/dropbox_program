//
// Created by udutta on 8/10/19.
//

#pragma once

#include <string>

namespace application {
namespace server {

class ServerMonitor {
public:
  virtual void saveFileInformation(const std::string &, uintmax_t) = 0;
  virtual void waitForFileFromClient(const std::string &, uintmax_t,
                                     const int &) = 0;
  virtual bool sendSendFileNameSizeResp(int connectedSocket) = 0;
  virtual bool handleInitialFileInformationMessage(int connectedSocket) = 0;

  virtual ~ServerMonitor() = default;
};

} // namespace server
} // namespace application
