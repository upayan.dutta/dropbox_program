//
// Created by udutta
//

#pragma once

#include <string>

namespace application {
namespace server {

class Connection {
public:
  virtual bool acceptClientConnection(std::string pathToScan) = 0;
};

} // namespace server
} // namespace application
