//
// Created by udutta
//

#pragma once

#include "Connection.hpp"
#include "ServerFactory.hpp"
#include "ServerMessageHandler.hpp"
#include <memory>

namespace application {
namespace server {

class ServerConnection : public Connection {
public:
  ServerConnection(std::shared_ptr<ServerFactory> factory) : factory{factory} {}
  bool acceptClientConnection(std::string pathToScan) override;

private:
  std::unique_ptr<MessageHandler> serverMessageHandler;
  std::shared_ptr<ServerFactory> factory;
};

} // namespace server
} // namespace application
