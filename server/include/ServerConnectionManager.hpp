//
// Created by udutta
//

#pragma once

#include "Connection.hpp"
#include "Manager.hpp"
#include "ServerFactory.hpp"
#include <memory>

namespace application {
namespace server {

class ServerConnectionManager : public Manager {

public:
  ServerConnectionManager(std::shared_ptr<ServerFactory> factory)
      : factory{factory} {}
  void start(std::string pathToScan) override;

private:
  std::unique_ptr<Connection> connectionManager;
  std::shared_ptr<ServerFactory> factory;
};

} // namespace server
} // namespace application