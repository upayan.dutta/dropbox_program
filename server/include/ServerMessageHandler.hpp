//
// Created by udutta
//

#pragma once

#include "MessageHandler.hpp"
#include "ServerDirectoryMonitor.hpp"
#include "ServerMessageDispatcher.hpp"
#include <map>
#include <memory>
#include <string>

namespace application {
namespace server {

enum class ListeningState { listen, stop };
using MessageDispatcherMap =
    std::map<std::string, std::unique_ptr<ServerMessageDispatcher>>;

class ServerMessageHandler : public MessageHandler {
public:
  ServerMessageHandler(int &socket, std::string pathToScan)
      : connectedSocket{socket} {
    std::shared_ptr serverDirectoryMonitor =
        std::make_shared<ServerDirectoryMonitor>(pathToScan);

    messageDispatcher["fileNameSizeReq"] =
        std::unique_ptr<ServerMessageDispatcher>(
            new FileNameSizeMessage(connectedSocket, serverDirectoryMonitor));

    messageDispatcher["initialFileInfoFromServerReq"] =
        std::unique_ptr<ServerMessageDispatcher>(
            new InitialFileInformationMessage(connectedSocket,
                                              serverDirectoryMonitor));
  }

  void initiateRecvLoop() override;

private:
  int &connectedSocket;
  ListeningState state{ListeningState::listen};
  MessageDispatcherMap messageDispatcher;

  void handleIncomingMsg(const std::string &msgType, const std::string &msg);
};

} // namespace server
} // namespace application
