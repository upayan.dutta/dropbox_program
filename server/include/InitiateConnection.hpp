//
// Created by udutta
//

#pragma once
#include "ServerConnectionManager.hpp"
#include "ServerFactory.hpp"
#include <iostream>

namespace application {
namespace server {
class InitiateConnection {
public:
  InitiateConnection(std::shared_ptr<ServerFactory> factory)
      : factory{std::move(factory)} {}

  void initiate(std::string pathToMonitor) {
    connectionManager = factory->createServerConnectionManager();

    connectionManager->start(pathToMonitor);
  }

private:
  std::unique_ptr<application::server::Manager> connectionManager;
  std::shared_ptr<ServerFactory> factory;
};
} // namespace server
} // namespace application
