//
// Created by udutta
//

#pragma once

#include "Connection.hpp"
#include "Manager.hpp"
#include "MessageHandler.hpp"
#include <memory>

namespace application {
namespace server {

class ServerFactory {
public:
  std::unique_ptr<Manager> createServerConnectionManager();
  std::unique_ptr<Connection> createServerConnection();
  std::unique_ptr<MessageHandler>
  createServerMessageHandler(int &socket, std::string pathToScan);
};

} // namespace server
} // namespace application
