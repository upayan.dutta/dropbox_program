//
// Created by udutta
//

#pragma once

namespace application {
namespace server {

class MessageHandler {
public:
  virtual void initiateRecvLoop() = 0;

  ~MessageHandler() = default;
};

} // namespace server
} // namespace application
