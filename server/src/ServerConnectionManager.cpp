//
// Created by udutta
//

#include "ServerConnectionManager.hpp"
#include "ServerConnection.hpp"

namespace application {
namespace server {

void ServerConnectionManager::start(std::string pathToScan) {
  connectionManager = factory->createServerConnection();

  connectionManager->acceptClientConnection(pathToScan);
}

} // namespace server
} // namespace application