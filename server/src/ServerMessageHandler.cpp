//
// Created by udutta
//

#include "ServerMessageHandler.hpp"
#include "SocketFunctions.hpp"
#include "Types.hpp"
#include "XmlMessageHandler.hpp"
#include <iostream>
#include <string>

namespace application {
namespace server {

void ServerMessageHandler::initiateRecvLoop() {
  std::cout << "initiating Client send loop" << std::endl;

  while (ListeningState::listen == state) {

    std::string receivedMessage = common::recvMessage(connectedSocket);

    common::XmlMessageHandler xmlMsgHandler;

    handleIncomingMsg(xmlMsgHandler.extractMessageType(receivedMessage),
                      receivedMessage);
  }
}

void ServerMessageHandler::handleIncomingMsg(const std::string &msgType,
                                             const std::string &msg) {
  auto itr = messageDispatcher.find(msgType);

  if (messageDispatcher.end() == itr) {
    std::cout << "!!! message dispatcher not found !!!" << std::endl;
    return;
  }

  itr->second->dispatch(msg);
}

} // namespace server
} // namespace application