//
// Created by udutta
//

#include "ServerDirectoryMonitor.hpp"
#include "SocketFunctions.hpp"
#include "XmlMessageHandler.hpp"
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <iterator>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

namespace application {
namespace server {

void ServerDirectoryMonitor::saveFileInformation(const std::string &fileName,
                                                 const uintmax_t fileSize) {

  std::cout << "Saved file name " << fileName << " and size " << fileSize
            << std::endl;
  fileInformation.emplace(std::make_pair(fileName, common::FileInfo{fileSize}));
}

void ServerDirectoryMonitor::waitForFileFromClient(const std::string &fileName,
                                                   const uintmax_t fileSize,
                                                   const int &connectedSocket)

{
  std::cout << "In waitForFileFromClient" << std::endl;

  char *buffer;
  if (NULL == (buffer = (char *)malloc(fileSize + 1))) {
    std::cout << "!!! Cannot assign enough memory for " << fileSize
              << " bytes !!!" << std::endl;
    return;
  }

  char *tmpBuffer = buffer;

  ssize_t valread = 0;
  ssize_t valReadCount = 0;
  uint16_t progressBar = 1;
  while (true) {
    valread = read(connectedSocket, tmpBuffer, fileSize);
    valReadCount += valread;
    tmpBuffer += valread;

    const float percentageRead =
        (static_cast<float>(valReadCount) / static_cast<float>(fileSize)) * 100;

    std::ostringstream os;
    std::fill_n(std::ostream_iterator<std::string>(os), progressBar++, "-");
    std::cout << os.str() << std::endl;

    std::cout << "Received " << percentageRead << " % from client" << std::endl;

    if ((-1 == valread) or (valReadCount >= fileSize)) {
      if (-1 == valread)
        std::cout << "!!! Read error. Exiting read loop !!!" << std::endl;
      break;
    }
  }

  createFile(buffer, fileName, fileSize);

  free(buffer);

  while (true) {
    if (sendFileUploadAckToClient(connectedSocket))
      break;
  }
}

void ServerDirectoryMonitor::createFile(char *buffer,
                                        const std::string &fileName,
                                        uintmax_t fileSize) {
  off_t off = 0;
  int toFd;

  errno = 0;
  std::string newFileName = directoryToMonitor + fileName;
  if ((toFd = open(newFileName.c_str(), O_WRONLY | O_CREAT | O_TRUNC,
                   S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0) {
    std::cout << "!!! File Open problem !!!" << std::endl;
    return;
  }
  ssize_t bytesWritten = 0;
  if ((bytesWritten = write(toFd, buffer, fileSize)) < 0) {
    std::cout << "!!! File write error !!!" << std::endl;
    return;
  } else {
    std::cout << "Actual written bytes: " << bytesWritten << std::endl;
  }

  close(toFd);
}

bool ServerDirectoryMonitor::sendFileUploadAckToClient(
    const int &connectedSocket) {
  std::cout << "In sendFileUploadAckToClient" << std::endl;

  common::XmlMessageHandler msgHandler;
  std::string msgToSend = msgHandler.createXmlTag("msgId", "fileUploadResp");

  return common::sendMessage(connectedSocket,
                             msgHandler.createFinalmessage(msgToSend));
}

bool ServerDirectoryMonitor::sendSendFileNameSizeResp(int connectedSocket) {

  common::XmlMessageHandler msgHandler;

  std::string msg = msgHandler.createXmlTag("msgId", "fileNameSizeResp");
  msg = msgHandler.createFinalmessage(msg);

  std::cout << "Sent msg: " << msg << std::endl;

  return common::sendMessage(connectedSocket, msg);
}

bool ServerDirectoryMonitor::handleInitialFileInformationMessage(
    int connectedSocket) {

  scanFilesInDirectory();
  sendInitialFileInfoFromServerResp(connectedSocket);

  return true;
}

bool ServerDirectoryMonitor::sendInitialFileInfoFromServerResp(
    int connectedSocket) {
  std::cout << "In sendInitialFileInfoFromServerResp" << std::endl;

  common::XmlMessageHandler xmlMsgCreator;

  const auto initialFileInfoFromServerRespMsg =
      xmlMsgCreator.createInitialFileInfoFromServerRespMsg(
          fileInformationToSendInInitialFileInformationMessage);

  common::sendMessage(connectedSocket, initialFileInfoFromServerRespMsg);

  return true;
}

void ServerDirectoryMonitor::scanFilesInDirectory() {

  file::directory_iterator end_itr;
  common::Md5Calculator md5Calc;

  for (auto entry : file::directory_iterator(directoryToMonitor)) {
    auto filename = entry.path().filename();
    if (file::is_regular_file(entry.status())) {

      saveFileInformationToBeSent(entry, filename, md5Calc);
    } else {
      std::cout << " ----Ignoring directory---- " << filename << std::endl;
    }
  }
}

std::uintmax_t
ServerDirectoryMonitor::getFileSize(const file::path &fileToCheck) {

  if (file::exists(fileToCheck)) {
    auto err = std::error_code{};
    auto filesize = file::file_size(fileToCheck, err);

    if (filesize != static_cast<uintmax_t>(-1))
      return filesize;
  } else {
    std::cout << "!!! File not found in server for calculating size !!!"
              << std::endl;
  }

  return static_cast<uintmax_t>(-1);
}

void ServerDirectoryMonitor::saveFileInformationToBeSent(
    const file::directory_entry &entry, const file::path &filename,
    common::Md5Calculator &md5Calc) {

  common::FileInfo serverFileInfo;
  serverFileInfo.md5Sum =
      md5Calc.calculateMd5(directoryToMonitor + filename.string());
  serverFileInfo.fileSize = getFileSize(entry);
  serverFileInfo.lastWriteTime =
      std::chrono::system_clock::to_time_t(file::last_write_time(entry));

  std::cout << "Saved file information in server: " << std::endl;
  std::cout << "File name: " << filename.string()
            << ", file size: " << serverFileInfo.fileSize
            << ", last modified time: " << serverFileInfo.lastWriteTime
            << " MD5: " << serverFileInfo.md5Sum << std::endl;

  fileInformationToSendInInitialFileInformationMessage.emplace(
      std::make_pair(filename.string(), serverFileInfo));
}

} // namespace server
} // namespace application