//
// Created by udutta
//

#include "InitiateConnection.hpp"
#include "ServerFactory.hpp"
#include <filesystem>
#include <iostream>

bool validatePath(std::string pathToScan);

int main(int argc, char **argv) {
  std::cout << "Hello World from server" << std::endl;

  if (2 != argc) {
    std::cout << "!!! Invalid number of arguments provided. Exiting server !!!"
              << std::endl;
    return 0;
  }

  std::string pathToMonitor = argv[1];
  std::cout << "Monitoring path: " << pathToMonitor << std::endl;

  if (not(validatePath(pathToMonitor))) {
    std::cout << "!!! Invalid path. Exiting server !!!" << std::endl;
    return 0;
  }

  std::shared_ptr<application::server::ServerFactory> factory =
      std::make_shared<application::server::ServerFactory>();

  std::unique_ptr<application::server::InitiateConnection> initiateConnection =
      std::make_unique<application::server::InitiateConnection>(factory);

  initiateConnection->initiate(pathToMonitor);

  return 0;
}

bool validatePath(std::string pathToScan) {
  const std::filesystem::path pathToShow{pathToScan};

  if (not std::filesystem::exists(pathToShow)) {
    std::cout << "!!! showFilesInDirectory: Input directory does not exists !!!"
              << std::endl;
    return false;
  }

  if (not std::filesystem::is_directory(pathToShow)) {
    std::cout << "!!! showFilesInDirectory: Input is not a directory !!!"
              << std::endl;
    return false;
  }
  return true;
}