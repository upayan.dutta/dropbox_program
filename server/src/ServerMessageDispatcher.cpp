//
// Created by udutta
//

#include "ServerMessageDispatcher.hpp"
#include <XmlMessageHandler.hpp>
#include <sstream>
#include <sys/socket.h>

namespace application {
namespace server {

void FileNameSizeMessage::dispatch(const std::string &msg) {
  std::cout << "--> Received message type: FileNameSizeMessage" << std::endl;
  common::XmlMessageHandler msgHandler;

  std::string fileName = msgHandler.getTagValue(msg, "fileName");
  std::cout << "File name: " << fileName << std::endl;
  std::stringstream fileSizeStr;
  fileSizeStr << msgHandler.getTagValue(msg, "fileSize");
  std::cout << "File size: " << fileSizeStr.str() << std::endl;

  uintmax_t fileSize;
  fileSizeStr >> fileSize;

  serverMonitor->saveFileInformation(fileName, fileSize);

  while (true) {
    if (serverMonitor->sendSendFileNameSizeResp(connectedSocket))
      break;
  }

  serverMonitor->waitForFileFromClient(fileName, fileSize, connectedSocket);
}

void InitialFileInformationMessage::dispatch(const std::string &msg) {

  std::cout << "--> Received message type: InitialFileInformationMessage"
            << std::endl;

  serverMonitor->handleInitialFileInformationMessage(connectedSocket);
}

} // namespace server
} // namespace application