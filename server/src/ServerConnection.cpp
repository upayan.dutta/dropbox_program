//
// Created by udutta
//

#include "ServerConnection.hpp"
#include "ConfigFileReader.hpp"
#include <iostream>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

namespace application {
namespace server {

bool ServerConnection::acceptClientConnection(std::string pathToScan) {
  int server_fd, new_socket;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);

  common::Config serverConfig = common::readConfigFile();

  std::cout << "### " << serverConfig.ipAdddress << std::endl;
  std::cout << "### " << serverConfig.port << std::endl;

  // Creating socket file descriptor
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt,
                 sizeof(opt))) {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(serverConfig.port);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }
  if (listen(server_fd, 3) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }
  if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                           (socklen_t *)&addrlen)) < 0) {
    perror("accept");
    exit(EXIT_FAILURE);
  }

  std::cout << "Client connected" << std::endl;

  serverMessageHandler =
      factory->createServerMessageHandler(new_socket, pathToScan);

  serverMessageHandler->initiateRecvLoop();

  return true;
}

} // namespace server
} // namespace application