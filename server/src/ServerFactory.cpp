//
// Created by udutta
//

#include "ServerFactory.hpp"
#include "ServerConnection.hpp"
#include "ServerConnectionManager.hpp"

namespace application {
namespace server {

std::unique_ptr<Manager> ServerFactory::createServerConnectionManager() {
  return std::make_unique<ServerConnectionManager>(
      std::make_shared<application::server::ServerFactory>(*this));
}

std::unique_ptr<Connection> ServerFactory::createServerConnection() {
  return std::make_unique<ServerConnection>(
      std::make_shared<application::server::ServerFactory>(*this));
}

std::unique_ptr<MessageHandler>
ServerFactory::createServerMessageHandler(int &socket, std::string pathToScan) {
  return std::make_unique<ServerMessageHandler>(socket, pathToScan);
}

} // namespace server
} // namespace application