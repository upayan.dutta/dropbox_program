//
// Created by udutta
//

#include "ClientConnection.hpp"
#include "ConfigFileReader.hpp"
#include <arpa/inet.h>
#include <iostream>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

namespace application {
namespace client {

bool ClientConnection::connectToServer(std::string path, std::string feature) {
  std::cout << "Starting ClientConnection::connect()" << std::endl;

  common::Config clientConfig = common::readConfigFile();

  std::cout << "### " << clientConfig.ipAdddress << std::endl;
  std::cout << "### " << clientConfig.port << std::endl;

  int sock = 0;
  struct sockaddr_in serv_addr;

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    std::cout << "Socket creation error" << std::endl;
    return false;
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(clientConfig.port);

  // Convert IPv4 and IPv6 addresses from text to binary form
  if (inet_pton(AF_INET, clientConfig.ipAdddress.c_str(),
                &serv_addr.sin_addr) <= 0) {
    std::cout << "Invalid address/ Address not supported" << std::endl;
    return false;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    std::cout << "Connection Failed" << std::endl;
    return false;
  }

  directoryMonitor = factory->createClientDirectoryMonitor(sock, path);

  directoryMonitor->startMonitor(feature);

  return true;
}
} // namespace client
} // namespace application