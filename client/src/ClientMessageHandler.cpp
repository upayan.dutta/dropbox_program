//
// Created by udutta
//

#include "ClientMessageHandler.hpp"
#include "SocketFunctions.hpp"
#include "Types.hpp"
#include "XmlMessageHandler.hpp"
#include <arpa/inet.h>
#include <cstring>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <unistd.h>

namespace application {
namespace client {

bool ClientMessageHandler::sendFileNameAndSizeMsg(
    file::path &filename, const std::uintmax_t &fileSize) {
  std::cout << "In sendFileNameAndSizeMsg" << std::endl;

  common::XmlMessageHandler msgCreator;

  std::string msg = msgCreator.createXmlTag("msgId", "fileNameSizeReq");
  msg = msg + msgCreator.createXmlTag("fileName", filename.string());

  std::ostringstream fileSizeStr;
  fileSizeStr << fileSize;
  msg = msg + msgCreator.createXmlTag("fileSize", fileSizeStr.str());

  std::string finalMsg = msgCreator.createFinalmessage(msg);

  std::cout << "Upayan: " << finalMsg << std::endl;

  return common::sendMessage(connectedSocket, finalMsg);
}

bool ClientMessageHandler::waitForFileInfoAck() {
  std::cout << "In waitForFileInfoAck() " << std::endl;

  common::XmlMessageHandler xmlMsgHdlr;
  std::string msgRcvd = common::recvMessage(connectedSocket);
  std::string msgType = xmlMsgHdlr.extractMessageType(msgRcvd);

  return (std::string::npos != msgType.find("fileNameSizeResp"));
}

bool ClientMessageHandler::waitForFileUploadAck() {
  std::cout << "In waitForFileUploadAck() " << std::endl;

  common::XmlMessageHandler msgHandler;

  std::string recvdMsg = common::recvMessage(connectedSocket);

  std::cout << "--> Received " << msgHandler.extractMessageType(recvdMsg)
            << std::endl;

  return true;
}

bool ClientMessageHandler::sendFileToServer(const std::string &filePath,
                                            const std::string &fileName,
                                            const std::uintmax_t fileSize) {
  int fromFile;
  off_t offset = 0;
  errno = 0;
  std::string fileToSend = filePath + "/" + fileName;
  if ((fromFile = open(fileToSend.c_str(), O_RDONLY)) < 0) {
    std::cout << "!!! File Open problem in sendFileToServer for file "
              << fileToSend << " ERROR: " << strerror(errno) << " !!! "
              << std::endl;
    return false;
  }
  auto numBytesSent =
      sendfile(connectedSocket /*to*/, fromFile /*from*/, &offset, fileSize);
  if (-1 == numBytesSent) {
    std::cout << "!!! sendFile ERROR !!!" << std::endl;
    std::cout << "Error number: " << strerror(errno) << std::endl;
    return false;
  } else {
    std::cout << "sendFile sent " << fileToSend << " file of size "
              << numBytesSent << " bytes -->" << std::endl;
  }

  close(fromFile);

  return true;
}

bool ClientMessageHandler::sendInitialFileInfoReqToServer() {
  std::cout << "In sendInitialFileInfoReqToServer" << std::endl;

  common::XmlMessageHandler xmlMsg;
  auto msgToSend = xmlMsg.createFinalmessage(
      xmlMsg.createXmlTag("msgId", "initialFileInfoFromServerReq"));

  if (not common::sendMessage(connectedSocket, msgToSend)) {
    std::cout << "!!! sendInitialFileInfoReqToServer failed !!! " << std::endl;
    return false;
  } else {
    std::cout << "sent sendInitialFileInfoReqToServer successfully -->"
              << std::endl;
  }
  return true;
}

FileInformation ClientMessageHandler::receiveInitialFileInfoReqToServerResp() {
  std::cout << "In receiveInitialFileInfoReqToServerResp() " << std::endl;

  std::string initialFileInfoFromServerRespMsg =
      common::recvMessage(connectedSocket);

  handleInitialFileInformationFromServer(initialFileInfoFromServerRespMsg);

  return initialFileInformationFromServer;
}

bool ClientMessageHandler::handleInitialFileInformationFromServer(
    const std::string &initialFileInfoMsg) {
  std::cout << "In handleInitialFileInformationFromServer" << std::endl;

  common::XmlMessageHandler xmlMsgHdlr;

  std::cout << "Received msg type: "
            << xmlMsgHdlr.extractMessageType(initialFileInfoMsg);

  saveInitialFileListReceivedFromServer(initialFileInfoMsg, xmlMsgHdlr);

  return true;
}

bool ClientMessageHandler::saveInitialFileListReceivedFromServer(
    const std::string &initialFileInfoMsg,
    const common::XmlMessageHandler &xmlMsgHdlr) try {

  std::cout << "In saveInitialFileListReceivedFromServer" << std::endl;

  std::stringstream numberOfFilesStr;
  numberOfFilesStr << xmlMsgHdlr.getTagValue(initialFileInfoMsg, "noOfFiles");

  uint32_t noOfFiles;

  numberOfFilesStr >> noOfFiles;

  initialFileInformationFromServer = xmlMsgHdlr.getFileList(
      xmlMsgHdlr.getTagValue(initialFileInfoMsg, "fileList"), noOfFiles);

  return true;
} catch (...) {
  std::cout << "!!! Exception caught in saveInitialFileListReceivedFromServer"
            << std::endl;
  return false;
}

} // namespace client
} // namespace application