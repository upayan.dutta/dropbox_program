//
// Created by udutta
//

#include "FileCompressor.hpp"
#include <fstream>
#include <iostream>

namespace application {
namespace client {

bool FileCompressor::compress(const std::vector<std::string> fileList) {

  for (const auto file : fileList) {
    std::ifstream fileToOpen;
    fileToOpen.open(file);

    std::string line;
    std::uintmax_t lineNumber = 1;
    while (std::getline(fileToOpen, line)) {
      std::cout << "Line: " << line << std::endl;
      fileLineData[line].emplace_back(common::FileLineInfo{file, lineNumber});
      lineNumber++;
    }
  }

  return true;
}

bool FileCompressor::print() {

  for (const auto &line : fileLineData) {
    std::cout << "########## Line : " << line.first << std::endl;
    for (const auto &file : line.second) {
      std::cout << "File: " << file.fileName << std::endl;
      std::cout << "Line Number: " << file.lineNumber << std::endl;
    }
  }

  return true;
}

} // namespace client
} // namespace application