//
// Created by udutta
//

#include "ClientFactory.hpp"
#include "ClientConnection.hpp"
#include "ClientConnectionManager.hpp"
#include "MessageHandler.hpp"
#include <memory>

namespace application {
namespace client {

std::unique_ptr<application::client::Manager>
ClientFactory::createClientConnectionManager() {
  return std::make_unique<ClientConnectionManager>(
      std::make_shared<application::client::ClientFactory>(*this));
}

std::unique_ptr<Connection> ClientFactory::createClientConnection() {
  return std::make_unique<ClientConnection>(
      std::make_shared<application::client::ClientFactory>(*this));
}

std::unique_ptr<ClientMonitor>
ClientFactory::createClientDirectoryMonitor(int &socket, std::string path) {
  return std::make_unique<DirectoryMonitor>(
      socket, path,
      std::make_shared<application::client::ClientFactory>(*this));
}

std::unique_ptr<MessageHandler>
ClientFactory::createClientMessageHandler(int &connectedSocket) {
  return std::make_unique<ClientMessageHandler>(connectedSocket);
}

} // namespace client
} // namespace application