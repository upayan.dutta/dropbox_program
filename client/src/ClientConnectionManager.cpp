//
// Created by udutta
//

#include "ClientConnectionManager.hpp"
#include "ClientConnection.hpp"

namespace application {
namespace client {

void ClientConnectionManager::start(std::string path, std::string feature) {
  connectionManager = factory->createClientConnection();

  connectionManager->connectToServer(path, feature);
}

} // namespace client
} // namespace application