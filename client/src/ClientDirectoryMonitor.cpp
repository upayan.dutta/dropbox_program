//
// Created by udutta
//

#include "ClientDirectoryMonitor.hpp"
#include "FileCompressor.hpp"
#include "Md5Calculator.hpp"
#include "Types.hpp"
#include <cstring>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace application {
namespace client {

void DirectoryMonitor::startMonitor(std::string feature) {

  clientMessageHandler = factory->createClientMessageHandler(connectedSocket);

  enabledFeature = feature;

  const file::path pathToShow{pathToScan};

  if (not isValidDirectoryPath(pathToShow))
    return;

  if (not initiateInitialFileInfoMsg())
    return;

  if (std::string::npos != enabledFeature.find("-onCompress"))
    compressFilesToSend(pathToShow);

  while (true) {
    scanFilesInDirectory(pathToShow);
    sleep(1);
  }
}

bool DirectoryMonitor::initiateInitialFileInfoMsg() {
  std::cout << "In initiateInitialFileInfoMsg" << std::endl;

  if (not(clientMessageHandler->sendInitialFileInfoReqToServer()))
    return false;

  fileInformation =
      clientMessageHandler->receiveInitialFileInfoReqToServerResp();

  return true;
}

bool DirectoryMonitor::isValidDirectoryPath(const file::path &pathToShow) {
  if (not file::exists(pathToShow)) {
    std::cout << "!!! showFilesInDirectory: Input directory does not exists !!!"
              << std::endl;
    return false;
  }

  if (not file::is_directory(pathToShow)) {
    std::cout << "!!! showFilesInDirectory: Input is not a directory !!!"
              << std::endl;
    return false;
  }
  return true;
}

void DirectoryMonitor::scanFilesInDirectory(const file::path &dirPath) {

  file::directory_iterator end_itr;

  for (auto entry : file::directory_iterator(dirPath)) {
    auto filename = entry.path().filename();
    auto fileSize = showFileSize(entry);
    if (file::is_regular_file(entry.status()) and (fileSize > 0)) {

      if (not checkIfFileNeedsToBeUploaded(entry, filename.string())) {
        std::cout << "File already successfully uploaded to server"
                  << std::endl;
        continue;
      }
      uploadFileToServer(entry, filename, fileSize);
    } else {
      std::cout << " ----Ignoring directory---- " << filename << std::endl;
    }
  }
}

void DirectoryMonitor::uploadFileToServer(const file::directory_entry &entry,
                                          file::path &filename,
                                          const std::uintmax_t fileSize) {

  time_t lastWriteTime =
      std::chrono::system_clock::to_time_t(file::last_write_time(entry));
  std::cout << " " << filename << ", " << fileSize
            << ", time: " << std::asctime(std::localtime(&lastWriteTime));

  std::stringstream ss;

  ss << filename.c_str() << ":" << fileSize;

  if (not clientMessageHandler->sendFileNameAndSizeMsg(filename, fileSize)) {
    return;
  }

  if (clientMessageHandler->waitForFileInfoAck()) {
    std::cout << "File Info ACK sent by server" << std::endl;
  } else {
    std::cout << "!!! File Info NACK received from server !!!" << std::endl;
    return;
  }

  if (not clientMessageHandler->sendFileToServer(pathToScan, filename,
                                                 fileSize)) {
    std::cout << "!!! Sending file to server failed !!!" << std::endl;
    return;
  }

  if (clientMessageHandler->waitForFileUploadAck()) {
    std::cout << "File uploaded to server successfully";
  } else {
    std::cout << "!!! File upload unsuccessful to server !!!" << std::endl;
    return;
  }

  updateClientFileInformationDatabase(filename, fileSize, lastWriteTime);
}

void DirectoryMonitor::updateClientFileInformationDatabase(
    file::path &filename, const std::uintmax_t &fileSize,
    const time_t lastWriteTime) {

  common::Md5Calculator md5Calc;

  const auto md5 = md5Calc.calculateMd5(pathToScan + filename.string());

  std::cout << "Inserting in client file database: filename: "
            << filename.string() << " filesize: " << fileSize
            << " last write time: " << lastWriteTime << " MD5: " << md5
            << std::endl;

  common::FileInfo fileInfo;

  fileInfo.isUploadedInServer = true;
  fileInfo.fileSize = fileSize;
  fileInfo.lastWriteTime = lastWriteTime;
  fileInfo.md5Sum = md5;

  fileInformation[filename.string()] = fileInfo;
}

bool DirectoryMonitor::checkIfFileNeedsToBeUploaded(
    const file::directory_entry &entry, const std::string &fileName)

{
  std::cout << "In checkIfFileNeedsToBeUploaded" << std::endl;

  FileInformation ::iterator itr = fileInformation.find(fileName);

  if (fileInformation.end() == itr) {
    std::cout << "!!! File " << fileName
              << " not found to check if successfully uploaded to "
                 "server !!!"
              << std::endl;
    return true;
  }

  const time_t lastWriteTime =
      std::chrono::system_clock::to_time_t(file::last_write_time(entry));

  if (lastWriteTime == itr->second.lastWriteTime) {
    std::cout << "File " << fileName
              << " not modified in client, no need to upload it in server"
              << std::endl;
    return false;
  }

  std::cout << "File " << fileName << " modified in client" << std::endl;

  return not checkIfFileAlreadyPresentInServer(entry, itr, true);
}

bool DirectoryMonitor::checkIfFileAlreadyPresentInServer(
    const file::directory_entry &entry, FileInformation::iterator &fileIter,
    const bool lastWriteTimeMismatch) {

  if ((not fileIter->second.isUploadedInServer) or (lastWriteTimeMismatch)) {
    return checkFileWriteTimeAndMd5Sum(entry, fileIter);
  } else {
    return true;
  }
}

bool DirectoryMonitor::checkFileWriteTimeAndMd5Sum(
    const file::directory_entry &entry, FileInformation::iterator &fileIter) {
  std::cout << "In checkFileWriteTimeAndMd5Sum" << std::endl;

  const time_t lastWriteTime =
      std::chrono::system_clock::to_time_t(file::last_write_time(entry));

  std::cout << "Client last write time: " << lastWriteTime << std::endl;
  std::cout << "Server last write time: " << fileIter->second.lastWriteTime
            << std::endl;

  if (lastWriteTime == fileIter->second.lastWriteTime) {
    std::cout << "Write time of file " << fileIter->first
              << " in client and server location are same" << std::endl;
    fileIter->second.isUploadedInServer = true;
    return true;
  }

  common::Md5Calculator md5Calculator;

  const auto md5 = md5Calculator.calculateMd5(pathToScan + fileIter->first);

  if (md5 == fileIter->second.md5Sum) {
    std::cout << "Md5 matches for both files in client and server directory"
              << std::endl;
    fileIter->second.isUploadedInServer = true;

    fileIter->second.lastWriteTime =
        lastWriteTime; // This will happen in case of already existing file in
                       // server and the lastWriteTime in client needs to be
                       // updated with actual time
    return true;
  } else {
    return false;
  }
}

std::uintmax_t DirectoryMonitor::showFileSize(const file::path &fileToCheck) {

  if (file::exists(fileToCheck)) {
    auto err = std::error_code{};
    auto filesize = file::file_size(fileToCheck, err);

    if (filesize != static_cast<uintmax_t>(-1))
      return filesize;
  }

  return static_cast<uintmax_t>(-1);
}

bool DirectoryMonitor::compressFilesToSend(const file::path &dirPath) {

  std::cout << "In compressFilesToSend" << std::endl;
  file::directory_iterator end_itr;
  FileCompressor fileCompressor;
  std::vector<std::string> fileList;

  for (auto entry : file::directory_iterator(dirPath)) {
    auto filename = entry.path().filename();
    auto fileSize = showFileSize(entry);
    if (file::is_regular_file(entry.status()) and (fileSize > 0)) {

      if (".txt" == entry.path().extension().string()) {
        std::cout << "*** File with extension txt found ***";
        std::cout << entry.path() << std::endl;
        fileList.emplace_back(entry.path());
      }
    }
  }
  fileCompressor.compress(fileList);
  fileCompressor.print();
  return true;
}

} // namespace client
} // namespace application