//
// Created by udutta
//

#include "ClientConnectionManager.hpp"
#include "ClientDirectoryMonitor.hpp"
#include "DirectorySyncManager.hpp"
#include "InitiateClientConnection.hpp"
#include <filesystem>
#include <iostream>

int main(int argc, char **argv) {
  std::cout << "Hello World from client" << std::endl;

  if (argc < 2) {
    std::cout << "!!! Invalid number of arguments provided. Exiting client !!!"
              << std::endl;
    return 0;
  }

  std::string featureOn;
  if (3 == argc) {
    featureOn = argv[2];
    if (std::string::npos == featureOn.find("-onCompress"))
      return 0;

    std::cout << "Feature On!!" << std::endl;
  }

  std::string pathToMonitor = argv[1];
  std::cout << "Monitoring path: " << pathToMonitor << std::endl;

  std::shared_ptr<application::client::ClientFactory> factory =
      std::make_shared<application::client::ClientFactory>();

  std::unique_ptr<application::client::InitiateClientConnection>
      initiateConnection =
          std::make_unique<application::client::InitiateClientConnection>(
              factory);

  initiateConnection->initiate(pathToMonitor, featureOn);

  std::cout << "Exiting client!!!" << std::endl;

  return 0;
}