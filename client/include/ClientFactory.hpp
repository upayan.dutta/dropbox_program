//
// Created by udutta
//

#pragma once

#include "Connection.hpp"
#include "Manager.hpp"
#include "MessageHandler.hpp"
#include "Monitor.hpp"
#include <memory>

namespace application {
namespace client {

class ClientFactory {
public:
  std::unique_ptr<Manager> createClientConnectionManager();
  std::unique_ptr<Connection> createClientConnection();
  std::unique_ptr<ClientMonitor> createClientDirectoryMonitor(int &socket,
                                                              std::string path);
  std::unique_ptr<MessageHandler>
  createClientMessageHandler(int &connectedSocket);
};

} // namespace client
} // namespace application
