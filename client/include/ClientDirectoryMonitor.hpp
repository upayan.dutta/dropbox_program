//
// Created by udutta
//

#pragma once

#include "ClientFactory.hpp"
#include "ClientMessageHandler.hpp"
#include "Monitor.hpp"
#include <filesystem>
#include <map>
#include <memory>

namespace application {
namespace client {

namespace file = std::filesystem;
using FileInformation = std::map<std::string, common::FileInfo>;

class DirectoryMonitor : public ClientMonitor {
public:
  DirectoryMonitor(int &socket, std::string path,
                   std::shared_ptr<ClientFactory> factory)
      : connectedSocket{socket}, pathToScan{path}, factory{factory} {}

  void startMonitor(std::string feature) override;

private:
  int &connectedSocket;
  std::unique_ptr<MessageHandler> clientMessageHandler;
  std::shared_ptr<ClientFactory> factory;
  FileInformation fileInformation;
  std::string pathToScan;
  std::string enabledFeature;

  void scanFilesInDirectory(const file::path &);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Uploads file in server using zero-copy
  ///////////////////////////////////////////////////////////////////////
  void uploadFileToServer(const file::directory_entry &, file::path &,
                          std::uintmax_t fileSize);

  std::uintmax_t showFileSize(const file::path &);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Check if the file is already present in server
  ///////////////////////////////////////////////////////////////////////
  bool checkIfFileAlreadyPresentInServer(const file::directory_entry &entry,
                                         FileInformation::iterator &fileIter,
                                         bool lastWriteTimeMismatch);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Check if the file needs to be uploaded to server
  ///////////////////////////////////////////////////////////////////////
  bool checkIfFileNeedsToBeUploaded(const file::directory_entry &entry,
                                    const std::string &fileName);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Update the file database of uploaded files in server,
  // after file is uploaded by client
  ///////////////////////////////////////////////////////////////////////
  void updateClientFileInformationDatabase(file::path &filename,
                                           const std::uintmax_t &fileSize,
                                           const time_t lastWriteTime);
  bool isValidDirectoryPath(const file::path &);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Sends initialFileInfoFromServerReq to server and receives
  // initialFileInfoFromServerRespMsg from server
  ///////////////////////////////////////////////////////////////////////
  bool initiateInitialFileInfoMsg();

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Check the MD5 sum and  last write time to verify
  // integrity of file between client and server
  ///////////////////////////////////////////////////////////////////////
  bool checkFileWriteTimeAndMd5Sum(const file::directory_entry &entry,
                                   FileInformation::iterator &);

  bool compressFilesToSend(const file::path &dirPath);
};

} // namespace client
} // namespace application