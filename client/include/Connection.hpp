//
// Created by udutta
//

#pragma once

#include <string>

namespace application {
namespace client {

class Connection {
public:
  virtual bool connectToServer(std::string path, std::string feature) = 0;
};

} // namespace client
} // namespace application