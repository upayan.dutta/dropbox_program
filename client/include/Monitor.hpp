//
// Created by udutta
//

#pragma once

#include <string>

namespace application {
namespace client {

class ClientMonitor {
public:
  virtual void startMonitor(std::string feature) = 0;

  virtual ~ClientMonitor() = default;
};

} // namespace client
} // namespace application