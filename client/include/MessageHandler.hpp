//
// Created by udutta
//

#pragma once

#include "Types.hpp"
#include <filesystem>
#include <map>
#include <string>

namespace application {
namespace client {

using FileInformation = std::map<std::string, common::FileInfo>;
namespace file = std::filesystem;

class MessageHandler {
public:
  virtual bool sendInitialFileInfoReqToServer() = 0;
  virtual FileInformation receiveInitialFileInfoReqToServerResp() = 0;
  virtual bool sendFileNameAndSizeMsg(file::path &filename,
                                      const std::uintmax_t &fileSize) = 0;
  virtual bool waitForFileInfoAck() = 0;
  virtual bool sendFileToServer(const std::string &filePath,
                                const std::string &fileName,
                                const uintmax_t fileSize) = 0;
  virtual bool waitForFileUploadAck() = 0;

  virtual ~MessageHandler() = default;
};

} // namespace client
} // namespace application
