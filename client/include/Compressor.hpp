//
// Created by udutta
//

#pragma once

#include <string>
#include <vector>

namespace application {
namespace client {

class Compressor {
public:
  virtual bool compress(std::vector<std::string>) = 0;
  virtual bool print() = 0;
};
} // namespace client
} // namespace application