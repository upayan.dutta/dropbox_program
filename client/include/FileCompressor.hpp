//
// Created by udutta
//

#pragma once

#include "Compressor.hpp"
#include "Types.hpp"
#include <list>
#include <map>

namespace application {
namespace client {

using FileLineData = std::map<std::string, std::list<common::FileLineInfo>>;

class FileCompressor : public Compressor {
public:
  bool compress(std::vector<std::string>) override;
  bool print() override;

private:
  FileLineData fileLineData;
};
} // namespace client
} // namespace application
