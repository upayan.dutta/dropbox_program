//
// Created by udutta
//

#pragma once

#include <string>

namespace application {
namespace client {

class Manager {
public:
  virtual void start(std::string path, std::string feature) = 0;

  virtual ~Manager() = default;
};

} // namespace client
} // namespace application