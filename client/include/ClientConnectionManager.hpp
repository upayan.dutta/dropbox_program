//
// Created by udutta
//

#pragma once

#include "../include/ClientFactory.hpp"
#include "Connection.hpp"
#include "Manager.hpp"
#include <memory>

namespace application {
namespace client {

class ClientConnectionManager : public Manager {
public:
  ClientConnectionManager(
      std::shared_ptr<application::client::ClientFactory> factory)
      : factory{factory} {}

  void start(std::string path, std::string feature) override;

private:
  std::unique_ptr<Connection> connectionManager;
  std::shared_ptr<application::client::ClientFactory> factory;
};

} // namespace client
} // namespace application
