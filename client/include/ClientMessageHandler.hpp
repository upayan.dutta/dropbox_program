//
// Created by udutta
//

#pragma once

#include "MessageHandler.hpp"
#include <map>
#include <string>

namespace common {
class XmlMessageHandler;
}

namespace application {
namespace client {

enum class SendState { send, stop };

class ClientMessageHandler : public MessageHandler {

public:
  ClientMessageHandler(int &socket) : connectedSocket{socket} {}

  //////////////////////////////////////////////////////////////////////
  // Purpose: Sends the file name and size message to server to get
  // prepared for receiving the file from client
  //////////////////////////////////////////////////////////////////////
  bool sendFileNameAndSizeMsg(file::path &filename,
                              const std::uintmax_t &fileSize) override;

  //////////////////////////////////////////////////////////////////////
  // Purpose: Waits till a file info ACK is received fro server,
  // so that the actual file transfer can begin
  //////////////////////////////////////////////////////////////////////
  bool waitForFileInfoAck() override;

  //////////////////////////////////////////////////////////////////////
  // Purpose: Waits till a file upload ACK is received from server
  //////////////////////////////////////////////////////////////////////
  bool waitForFileUploadAck() override;

  //////////////////////////////////////////////////////////////////////
  // Purpose: Uploads file to server using zero-copy mechanism
  //////////////////////////////////////////////////////////////////////
  bool sendFileToServer(const std::string &filePath,
                        const std::string &fileName, const uintmax_t fileSize) override;

  //////////////////////////////////////////////////////////////////////
  // Purpose: Sends initialFileInfoFromServerReq to server
  //////////////////////////////////////////////////////////////////////
  bool sendInitialFileInfoReqToServer() override;

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Receives initialFileInfoFromServerRespMsg from server
  ///////////////////////////////////////////////////////////////////////
  FileInformation receiveInitialFileInfoReqToServerResp() override;

private:
  int &connectedSocket;
  FileInformation initialFileInformationFromServer;

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Save initialFileInfoFromServerRespMsg from server
  ///////////////////////////////////////////////////////////////////////
  bool
  handleInitialFileInformationFromServer(const std::string &initialFileInfoMsg);

  ///////////////////////////////////////////////////////////////////////
  // Purpose: Save initialFileInfoFromServerRespMsg from server
  ///////////////////////////////////////////////////////////////////////
  bool saveInitialFileListReceivedFromServer(
      const std::string &initialFileInfoMsg,
      const common::XmlMessageHandler &xmlMsgHdlr);
};

} // namespace client
} // namespace application
