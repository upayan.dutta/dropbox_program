//
// Created by udutta
//

#pragma once

#include "ClientDirectoryMonitor.hpp"
#include "ClientFactory.hpp"
#include "ClientMessageHandler.hpp"
#include "Connection.hpp"
#include <bits/unique_ptr.h>

namespace application {
namespace client {

class ClientConnection : public Connection {
public:
  ClientConnection(std::shared_ptr<ClientFactory> factory) : factory{factory} {}

  bool connectToServer(std::string path, std::string feature) override;

private:
  std::unique_ptr<ClientMonitor> directoryMonitor;
  std::shared_ptr<ClientFactory> factory;
};

} // namespace client
} // namespace application