//
// Created by udutta
//

#pragma once
#include "ClientConnectionManager.hpp"
#include "ClientFactory.hpp"
#include <iostream>

namespace application {
namespace client {

class InitiateClientConnection {
public:
  InitiateClientConnection(
      std::shared_ptr<application::client::ClientFactory> factory)
      : factory{std::move(factory)} {}

  void initiate(std::string path, std::string feature) {
    connectionManager = factory->createClientConnectionManager();

    connectionManager->start(path, feature);
  }

private:
  std::unique_ptr<application::client::Manager> connectionManager;
  std::shared_ptr<application::client::ClientFactory> factory;
};
} // namespace client
} // namespace application
