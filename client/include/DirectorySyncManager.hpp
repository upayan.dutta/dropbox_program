//
// Created by udutta
//

#pragma once

#include "Md5Calculator.hpp"
#include <memory>

namespace application {
namespace client {

class DirectorySyncManager {

public:
  void calculateMd5();

private:
  std::unique_ptr<common::Md5Calculator> md5Calculator;
};

} // namespace client
} // namespace application